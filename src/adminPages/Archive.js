import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
//import { Navigate } from 'react-router-dom';
import { useParams, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import ProductCard from '../components/ProductCard';
import { TbCakeOff } from "react-icons/tb"
import { TbCake } from "react-icons/tb"
import { HiBackspace } from "react-icons/hi"



export default function Archive(){

  const {user} = useContext(UserContext);
  const { productId, name } = useParams();
  const navigate = useNavigate();


  const toggleArchive = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
      //body: JSON.stringify({ isActive: false }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if(data){
          Swal.fire({
            title: "Archive Successful",
            icon: "success",
            text: `Product is now inactive`
          })
        }
      });
  };

  const toggleUnarchive = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/unarchive`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        "Authorization": `Bearer ${localStorage.getItem('token')}`
      },
     // body: JSON.stringify({ isActive: true }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if(data){
          Swal.fire({
            title: "Unarchive Successful",
            icon: "success",
            text: `Product is now active`
          })
        }
      });
  };

  const getAllProducts =() =>{
    navigate('/products')

}
  return (
    <>
    <Button className="bg-warning text-dark m-2 p-2" variant='dark' size='lg' style={{fontWeight: 'bold'}} onClick={toggleArchive}><TbCakeOff />Archive</Button>
    <Button className="bg-warning text-dark m-2 p-2" variant='dark' size='lg' style={{fontWeight: 'bold'}} onClick={toggleArchive}><TbCake />Unarchive</Button>
    <Button className='bg-warning text-dark mx-4' variant='dark' size='lg' style={{fontWeight: 'bold'}} onClick={getAllProducts}><HiBackspace />Back</Button>


    </>
  )  
}




