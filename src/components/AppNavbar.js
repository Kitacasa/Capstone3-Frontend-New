import { useContext, useEffect } from 'react';
//React bootstrap components
import { Navbar, Nav } from 'react-bootstrap';
//react-router
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import React from 'react';

import { GiCupcake } from "react-icons/gi";
import { AiOutlineShoppingCart } from "react-icons/ai";
import { GiCakeSlice } from "react-icons/gi";
import { AiOutlineHome } from "react-icons/ai";
import { BiLogOut } from "react-icons/bi";
import { MdProductionQuantityLimits } from "react-icons/md";
import { BiLogInCircle } from "react-icons/bi";
import { BiRegistered } from "react-icons/bi";
import { FaUserLock } from "react-icons/fa"

export default function AppNavbar() {


	const { user, setUser } = useContext(UserContext);


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			if(data._id !== undefined){
				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			} else{
				setUser({
					id: null,
					isAdmin: null
				})
			}
			
		})
		
	}, [])
	

	return(

		
		<Navbar expand="lg" variant="light" className="m-2 ">
			<Navbar.Brand className="ms-4" as={Link} to="/" ><h1><GiCupcake />{`JK's CAKES & BAKES`}</h1>  </Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ms-auto">
					<Nav.Link as={Link} to="/"><h6><AiOutlineHome /> {`𝗛𝗼𝗺𝗲`}</h6></Nav.Link>
					{
						user.isAdmin ?
						<>
                                <Nav.Link as={Link} to='/admin'><h6><FaUserLock /> {`Admin`}</h6></Nav.Link>
								
						</>
                        	:
					
								<Nav.Link as={Link} to="/menu"><h6><GiCakeSlice /> {`𝗣𝗿𝗼𝗱𝘂𝗰𝘁𝘀`}</h6></Nav.Link>
					}

					{(user.id !== null)
					  ? 
					  <>
					        <Nav.Link as={Link} to="/cart"><h6><AiOutlineShoppingCart/> {`𝗖𝗮𝗿𝘁`} </h6></Nav.Link>
					        <Nav.Link as={Link} to="/myOrders"><h6><MdProductionQuantityLimits/> {`𝗢𝗿𝗱𝗲𝗿𝘀`}</h6></Nav.Link>
							<Nav.Link as={Link} to="/logout"><h6><BiLogOut /> {`𝗟𝗼𝗴𝗼𝘂𝘁`}</h6></Nav.Link>
							
							</>
					  :
					  <>
							<Nav.Link as={Link} to="/login"><h6><BiLogInCircle /> {`𝗟𝗼𝗴𝗶𝗻`} </h6></Nav.Link>
							<Nav.Link as={Link} to="/register"><h6><BiRegistered /> {`𝗥𝗲𝗴𝗶𝘀𝘁𝗲𝗿`} </h6></Nav.Link>
						</>
					
					    
					}

				</Nav>
			</Navbar.Collapse>
		</Navbar>

		)
}


